import unittest
from src.jogo import *

class TestJogo(unittest.TestCase):
    def test_quando_numero_2_entao_retorna_2(self):
        assert jogo(2) == 2

    def test_quando_numero_3_entao_retorna_queijo(self):
        assert jogo(3) == 'queijo'

    def test_quando_numero_5_entao_retorna_goiabada(self):
        assert jogo(5) == 'goiabadaa'

    def test_quando_numero_15_entao_retorna_romeu_e_julieta(self):
        self.assertEqual(jogo(15), 'romeu e julieta')
