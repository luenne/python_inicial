from dynaconf import Dynaconf

""" Lendo arquivos de configuração """
settings = Dynaconf(load_dotenv=True, environments=True, settings_files=['../config/settings.toml', '../config/settings.json'])

if __name__ == '__main__':
    print('Escrita arquivo de configuração .env')
    print(settings.ENV)
    
    print('Escrita arquivo de configuração TOML')
    print(settings.envToml)
    
    print('Escrita arquivo de configuração JSON')
    print(settings.envjson)