import argparse
from jogo import *

def cli():
    if __name__ == '__main__':
        parser = argparse.ArgumentParser()
        parser.add_argument('--n', type=int, required=True)

        args = parser.parse_args()

        print(jogo(args.n))
        
cli()