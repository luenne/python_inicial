from src.jogo import *

def test_quando_numero_2_entao_retorna_2():
    assert jogo(2) == 2

def test_quando_numero_3_entao_retorna_queijo():
    assert jogo(3) == 'queijo'

def test_quando_numero_5_entao_retorna_goiabada():
    assert jogo(5) == 'goiabada'

def test_quando_numero_15_entao_retorna_romeu_e_julieta():
    assert jogo(15) == 'romeu e julieta'
