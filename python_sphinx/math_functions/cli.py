from math_function import *
import argparse

"""Inicial"""
def cli():
    """ Espera receber o tipo de função e os valores via terminal.

    :--f: refere-se ao tipo de função: sum (soma), pow (potência) e diff (subtração)
    :INTEGERS: os valores inteiros (separados por espaço)
    """
    if __name__ == '__main__':
        parser = argparse.ArgumentParser()
        parser.add_argument('integers', metavar='N', type=int, nargs="+")
        parser.add_argument('--f', type=str, required=True)
        
        args = parser.parse_args()
        if args.f == 'pow':
            result = pow_value(args.integers[0], args.integers[0])
            print('Potência de ' + str(args.integers[0]) + '^' + str(args.integers[0]) + ' = ' + str(result))
    
    
cli()