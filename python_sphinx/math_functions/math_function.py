"""Operações Matemáticas"""

def pow_value(exp, base):
    """ Responsável pelo retorno da potência.

    :exp: Primeiro argumento e refere-se ao expoente
    :base: Segundo argumento e refere-se à base
    :return: O valor do retorno é o resultado da potência.
    """

    result = 1
    it = 0
    while it < exp:
        result *= base
        it = it + 1
    
    return result

def sum_value(value1, value2):
    """ Responsável pelo retorno da soma.

    :value1: Primeiro argumento.
    :value2: Segundo argumento.
    :return: O valor do retorno é o resultado da soma.
    """

    return value1 + value2

def diff_value(value1, value2):
    """ Responsável pelo retorno da subtração.

    :value1: Primeiro argumento.
    :value2: Segundo argumento.
    :return: O valor do retorno é o resultado da subtração.
    """

    return value1 + value2