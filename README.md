# Ferramentas Python



## Sphinx

Sphinx é um gerador de documentação ou uma ferramenta que traduz um conjunto de arquivos fonte de texto simples em vários formatos de saída, pode gerar arquivos HTML, PDF (por meio do LATEX), entre outros. Utiliza um formato de marcação de texto o [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html), bem simples.

### Instalação ferramenta

```
pip install -U Sphinx
```

### Inicialização Estrutura do diretório de documentação

```
$ sphinx-quickstart

Você tem duas opções para definir o diretório de compilação para a saída
Sphinx. Você pode usar um diretório "_build" no caminho raiz ou separar
os diretórios de "origem" e "compilação" no caminho raiz.
> Separar os diretórios de origem e compilação (y/n) [n]: **[enter]**

O nome do projeto vai aparecer em vários lugares na documentação compilada.
> Nome do projeto: 
> Nome(s) de autor(es): 
> Lançamento do projeto []: 

Se os documentos forem escritos em um idioma diferente do inglês, você
pode selecionar um idioma aqui pelo seu código de idioma. O Sphinx,
então, traduzirá o texto gerado para esse idioma.

Para obter uma lista dos códigos suportados, consulte
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
> Idioma do projeto [en]: **pt_BR**
```

Abra o arquivo `[conf.py]` e retire o comentário do trecho:

```python
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

extensions = ['sphinx.ext.autodoc']
```

Deve ser passado o caminho absoluto dos arquivos `.py`. E sobrescrever `extensions = ['sphinx.ext.autodoc']`.

Abra o arquivo `index.rst` e adicione (para habilitar o uso de vários módulos):

```
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   **modules**
```

### Geração do documento

```
$ sphinx-apidoc -o <OUTPUT_PATH> <MODULE_PATH>
```

- **OUTPUT_PATH**: diretório onde as fontes geradas são colocadas.
- **MODULE_PATH**: caminho com o pacote dos arquivos Python que serão documentados.

### Documento html

Gera os arquivos `html` na pasta `_build/html`.

```
$ make html
```

### Alteração de tema para documentação

```
$ pip install sphinx_rtd_theme
```

Alterar no `conf.py`:

```python
# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
```

## Unittest

[Unittest](https://docs.python.org/3/library/unittest.html) É um framework de testes unitários nativo do Python, foi originalmente inspirado no JUnit. Ele suporta a automação de testes, compartilhamento de configuração e código de desligamento para testes, agregação de testes em coleções e independência dos testes do framework de relatórios.

Para a criação de um caso de teste é preciso criar uma classe que estende `unittest.TestCase` e todos os testes são definidos por métodos que começam com `test`. Esta convenção na nomenclatura informa o runner a respeitos de quais métodos são, na verdade, testes.

Para execução dos testes desse exemplo do projeto `python_unittest`, sem ter problemas com os diretórios é só executar

```
$ python -m tests/jogo_test.py
```

## Pytest
O [Pytest](https://docs.pytest.org/en/7.1.x/) é um frameword de teste do Python que fornece soluções para execução de testes e diversas validações, pode executar testes do próprio `unittest`.

O passo a passo da instalação pode ser encontrado [aqui](https://docs.pytest.org/en/7.1.x/getting-started.html?highlight=install).
